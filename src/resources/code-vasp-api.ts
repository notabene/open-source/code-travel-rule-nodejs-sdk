import { CodeTravelRuleConfig } from '../code-travel-rule';
import {
  convertPublicKey,
  convertSecretKey,
  decrypt,
  verify,
  getPublicKey,
  getVerifyKey,
  getKeysFromSeed,
} from '../utils/code-crypto';
import { VirtualAssetAddressSearchEncryptedBody } from './code-api';

export default class CodeVaspAPI {
  private vaspKey;

  constructor(config: CodeTravelRuleConfig) {
    this.vaspKey = getKeysFromSeed(config.vaspSecretSeed);
  }

  /**
   * Checks if CODE signature is valid
   * @param headers
   * @param body
   */
  checkSignature(headers: CodeHttpHeaders, body?: any) {
    const datetime = headers['X-Code-Req-Datetime'];
    const nonce = Number.parseInt(headers['X-Code-Req-Nonce']);
    const xCodeReqPubkey = headers['X-Code-Req-Pubkey'];
    const xCodeReqSignature = headers['X-Code-Req-Signature'];

    const datetimeBuffer = Buffer.from(datetime, 'utf8');
    const nonceBuffer = new Uint8Array(
      new Uint32Array([nonce]).buffer
    ).reverse();

    const bodyBuffer = Buffer.from(body, 'utf8');
    const toSignArray = Buffer.concat([
      datetimeBuffer,
      bodyBuffer,
      nonceBuffer,
    ]);

    const verifyKey = getVerifyKey(Buffer.from(xCodeReqPubkey, 'base64'));

    const pubKey = getPublicKey(verifyKey);
    return verify(
      toSignArray,
      Buffer.from(xCodeReqSignature, 'base64'),
      pubKey
    );
  }

  virtualAssetAddressSearch_decrypt(
    headers: CodeHttpHeaders,
    body: VirtualAssetAddressSearchEncryptedBody
  ) {
    const peerPublicKey = headers['X-Code-Req-Pubkey'];
    const privateKey = this.vaspKey.secretKey;

    //Convert signing keys to encryption keys
    const theirDHPublicKey = convertPublicKey(peerPublicKey);
    if (!theirDHPublicKey) {
      throw Error('Error converting public key to DH public');
    }
    const myDHSecretKey = convertSecretKey(privateKey);

    //Decrypt payload
    const decryptedPayload = decrypt(
      body.payload,
      theirDHPublicKey,
      myDHSecretKey
    );
    return { currency: body.currency, payload: JSON.parse(decryptedPayload) };
  }
}

export type CodeHttpHeaders = {
  'X-Code-Req-Datetime': string;
  'X-Code-Req-Nonce': string;
  'X-Code-Req-Pubkey': string;
  'X-Code-Req-Signature': string;
  'X-Request-Origin': string;
};
