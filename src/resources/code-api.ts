import { AxiosInstance } from 'axios';
import { CodeTravelRuleConfig } from '../code-travel-rule';
import {
  convertPublicKey,
  convertSecretKey,
  encrypt,
  getKeysFromSeed,
} from '../utils/code-crypto';

export default class CodeAPI {
  private http: AxiosInstance;
  private vaspKey;

  constructor(http: AxiosInstance, config: CodeTravelRuleConfig) {
    this.http = http;
    this.vaspKey = getKeysFromSeed(config.vaspSecretSeed);
  }

  /**
   * VASP List Search
   *
   * This returns a list of all VASPs that are interoperated with CODE for operation.
   * https://codevasp.gitbook.io/code-api-doc-en/code-api/vasp-list-search
   *
   * @returns vasps . Array of VASPs. Each VASP has the following fields: vaspName, pubkeys, health, vaspEntityId, countryOfRegistration, allianceName and domesticVasp
   */
  public async vaspListSearch(): Promise<VaspListSearchResult> {
    return await this.http.get('/vasps');
  }

  /**
   * Public Key List Search
   *
   * This returns a list of VASP public keys corresponding to VaspEntityId.
   *  https://codevasp.gitbook.io/code-api-doc-en/code-api/public-key-list-search
   *
   * @param vaspEntityId the VaspEntityId of the VASP, which wants to obtain a list of public keys.
   * @returns pubkeys. Array of Public Keys for a VASP. Each entry has the following fields: expiresAt, pubkey
   */
  public async publicKeyListSearch(
    vaspEntityId: string
  ): Promise<PublicKeyListSearchResult> {
    return await this.http.get(`/Vasp/${vaspEntityId}/pubkey`);
  }

  /**
   * Virtual Asset Address Search
   *
   * The virtual asset address, which will transfer an asset, requests to confirm whether the asset is owned by
   *  the VASP within the CODE or not.
   *  https://codevasp.gitbook.io/code-api-doc-en/code-api/virtual-asset-address-search
   *
   * A VASP who wants to transfer assets need to know which VASP owns the address (address + tag(optional)) of
   *  a virtual asset to which a user wants to transfer his or her asset by using this API in the first step
   *  of the entire process.
   * You can skip this step if you already know which VASP's address it is, but it is recommended that you use
   *  this API to verify that the address of the virtual asset, to which you want to transfer an asset, belongs
   *  to a known VASP.
   * This API operates in the following two ways depending on whether Path Parameter (BeneficiaryVaspEntityId)
   *  exists or not.
   * If there is no Path Parameter,
   *  a request is sent to all VASPs, and responses are collected to be returned the requesting VASP.
   *  Since if this is used too frequently, this affects the overall performance of the system, a call cannot
   *  be made at more than 30 times per minute from one IP.
   * If there is Path Parameter,
   *  only the corresponding VASP receives the request.
   *
   * @param vaspEntityId This is the EntityID of the VASP which owns the address to which the asset
   *
   * @param body.currency: This is a symbol of the virtual asset you want to transfer. (This is case insensitive.)
   * @param body.payload: This is an object containing the ivms101 message with personal information
   * @param body.payload.ivms101:
   * @param body.payload.ivms101.Beneficiary: This is an object to contain the beneficiary's name and
   *                                          wallet address information
   * @param body.payload.ivms101.Beneficiary.beneficiaryPersons: (Optional) In case of inquiry by designating an
   *                                                              exchange, verification may be requested by
   *                                                              sending the addresse's name. Please refer to
   *                                                              the message format of the asset transfer
   *                                                              permission request API.
   * @param body.payload.ivms101.Beneficiary.accountNumber: (Required): This is a virtual asset address from
   *                                                          which the asset is transferred. A secondary
   *                                                          addresses such as tag or memo are added after
   *                                                          the ':' delimiter.
   *                                is transferred. If you do not know the target VASP, do not enter it.
   *
   * @returns
   */
  public async virtualAssetAddressSearch(
    vaspEntityId: string,
    body: VirtualAssetAddressSearchBody
  ): Promise<VirtualAssetAddressSearchResponse> {
    //Encrypt only the payload
    const { encryptedPayloadStr, peerPubKey } = await this.encryptPayload(
      vaspEntityId,
      body.payload
    );
    (body as unknown as VirtualAssetAddressSearchEncryptedBody).payload =
      encryptedPayloadStr;
    const bodyStr = JSON.stringify(body);

    return await this.http.post(`/VerifyAddress/${vaspEntityId}`, bodyStr, {
      headers: { 'X-Code-Req-Remote-PubKey': peerPubKey },
    });
  }

  /**
   * Asset Transfer Authorization
   *
   * Request an authorization for the transfer of virtual assets from the VASP to which the assets were transferred.
   *  https://codevasp.gitbook.io/code-api-doc-en/code-api/asset-transfer-authorization
   *
   *  A VASP who wants to transfer an asset requests the VASP to whom the virtual asset is to be transferred for an authorization to transfer the asset. The VASP to transfer assets sends the sender's personal information at the request, and the VASP, to whom an asset is transferred, can check the sender's information and reject the transaction or authorize it with the beneficiary's personal information.
   *   The VASP who sent a request updates the status of saved asset transfer list to verified or denied before sending the request or after receiving the response.
   *
   * @param vaspEntityId This shall be entered as the EntityID of the VASP that owns the address to which the asset is transferred.
   * @returns
   */
  public async assetTransferAuthorization(
    vaspEntityId: string,
    body: AssetTransferAuthorizationBody
  ): Promise<AssetTransferAuthorizationResponse> {
    //Encrypt only the payload
    const { encryptedPayloadStr, peerPubKey } = await this.encryptPayload(
      vaspEntityId,
      body.payload
    );
    (body as any).payload = encryptedPayloadStr;
    const bodyStr = JSON.stringify(body);
    console.log(bodyStr);

    return await this.http.post(`/transfer/${vaspEntityId}`, bodyStr, {
      headers: { 'X-Code-Req-Remote-PubKey': peerPubKey },
    });
  }

  /**
   * Report Transfer Result (TX Hash)
   *
   * The blockchain sends the result (Transaction ID) after the transfer of the asset has been executed.
   *  https://codevasp.gitbook.io/code-api-doc-en/code-api/report-transfer-result-tx-hash
   *
   *  A VASP who transfers the virtual asset shall send the transfer result (Transaction ID) of the corresponding asset to the VASP to whom the asset is transferred immediately after transferring an asset, and use this API.
   *  This API shall be called at a time when it can be guaranteed that the transfer of a virtual asset will not be rolled back. For example, it may be the time when an asset is transferred, txid is created on the block chain, and a certain number of Confirms are checked.
   *
   * @param vaspEntityId This shall be entered as the EntityID of the VASP who owns the address to which the asset is transferred.
   * @returns
   */
  public async reportTransferResult(
    vaspEntityId: string,
    body: ReportTransferResultBody
  ) {
    const bodyStr = JSON.stringify(body);
    return await this.http.post(`/transfer/${vaspEntityId}/txid`, bodyStr);
  }

  /**
   * Transaction status search
   *
   * Request the blockchain transaction status of a specific asset transfer detail to the originating VASP.
   *  https://codevasp.gitbook.io/code-api-doc-en/code-api/transaction-status-search
   *
   *  Sometimes, a VASP who has been transferred an asset needs to know the status of the blockchain transaction of the VASP who transferred the asset.
   *   This API is not an API to look up the asset transfer details of the counterpart VASP, but the API for the beneficiary VASP to know the blockchain transaction status of the originating VASP. This can be used in the followings.
   *   When the beneficiary VASP wants to check if the txid has been updated in the asset transfer details before an asset transfer transaction occurs on the blockchain and is reflected in the user's asset information, but if the status of its own asset transfer information is not confirmed, or wants to request the transaction status of an asset transfer to the originating VASP.
   *   When a response for an asset transfer authorization request has been sent, but the asset was not transferred, or the VASP wants to request the transaction status of the corresponding asset transfer to the originating VASP.
Note) Please note that the behavior has changed in the existing API specification. This is an API that looks up the transaction status, not the asset transfer details.
   *
   * @param originatingVaspEntityId This shall be entered as the EntityID of the VASP who owns the address to which the asset is transferred.
   * @returns
   */
  public async transactionStatusSearch(
    originatingVaspEntityId: string,
    body: TransactionStatusSearchBody
  ) {
    const bodyStr = JSON.stringify(body);
    return await this.http.post(
      `/transfer/${originatingVaspEntityId}/status`,
      bodyStr
    );
  }

  /**
   * Finish Transfer
   *
   * Request the counterpart VASP to change the status of the asset transfer details.
   *   https://codevasp.gitbook.io/code-api-doc-en/code-api/finish-transfer
   *
   *  This API is used in the following cases:
   *   When the VASP who transfers an asset received an allow response for the address search request, but when notifying the beneficiary VASP that it terminates the process without transferring the assets by its internal judgment.
   *   The use of this API is not compulsory, but please consider using this in terms of notifying the other VASP of the final status of the asset transfer process.
   *
   * @param originatingVaspEntityId This shall be entered as the EntityID of the VASP that owns the address to which the asset was transferred.
   * @returns
   */
  public async finishTransfer(
    originatingVaspEntityId: string,
    body: FinishTransferBody
  ) {
    const bodyStr = JSON.stringify(body);
    return await this.http.put(
      `/transfer/${originatingVaspEntityId}/status`,
      bodyStr
    );
  }

  /**
   * [PRIVATE] Gets the active public key for a VASP
   *
   * @param vaspEntityId
   * @returns
   */
  private async getPeerPublicKey(
    vaspEntityId: string
  ): Promise<VaspPublicKey | undefined> {
    const apiResp = await this.publicKeyListSearch(vaspEntityId);
    const today = new Date().getTime();
    const foundedKey = apiResp.pubkeys.find(
      (pbk: { expiresAt: string }) => Date.parse(pbk.expiresAt) > today
    );
    return foundedKey;
  }

  /**
   * [PRIVATE] Encrypt payload
   *
   * @param vaspEntityId
   * @returns
   */
  private async encryptPayload(vaspEntityId: string, payload: object) {
    // Get the public key for the remote VASP
    const peerKey = await this.getPeerPublicKey(vaspEntityId);
    if (!peerKey) {
      throw Error('PublicKey for ' + vaspEntityId + 'not found');
    }

    //Convert signing keys to encryption keys
    const theirDHPublicKey = convertPublicKey(peerKey.pubkey);
    if (!theirDHPublicKey) {
      throw Error(
        'Error converting public key to DH public key for:' + vaspEntityId
      );
    }
    const myDHSecretKey = convertSecretKey(this.vaspKey.secretKey);

    const payloadStr = JSON.stringify(payload);

    const encryptedPayload = encrypt(
      payloadStr,
      theirDHPublicKey,
      myDHSecretKey
    );

    const encryptedPayloadStr =
      Buffer.from(encryptedPayload).toString('base64');
    const peerPubKey = peerKey.pubkey;
    return {
      encryptedPayloadStr,
      peerPubKey,
    };
  }
}

// TYPES

export type VaspListSearchResult = {
  vasps: VaspListEntry[];
};

export type VaspListEntry = {
  allianceName: string; // Maybe an ENUM
  countryOfRegistration: string; //Two letter code
  health: string; //ENUM maybe?
  pubkeys: VaspPublicKey[];
  vaspEntityId: string;
  vaspName: string;
};

export type VaspPublicKey = {
  expiresAt: string;
  pubkey: string;
};

export type PublicKeyListSearchResult = {
  pubkeys: VaspPublicKey[];
};

export type VirtualAssetAddressSearchBody = {
  currency: string;
  payload: {
    ivms101: {
      Beneficiary: {
        beneficiaryPersons: any[];
        accountNumber: string[];
      };
    };
  };
};

export type VirtualAssetAddressSearchEncryptedBody = {
  currency: string;
  payload: string;
};

export enum VirtualAssetAddressSearchResult {
  valid = 'valid',
  invalid = 'invalid',
}
export enum VirtualAssetAddressSearchInvalidReasonType {
  NOT_FOUND_ADDRESS = 'NOT_FOUND_ADDRESS', //This is a case where a virtual asset address cannot be found.
  NOT_SUPPORTED_SYMBOL = 'NOT_SUPPORTED_SYMBOL', //This is a currency symbol which cannot be traded.
  NOT_KYC_USER = 'NOT_KYC_USER', //This is a case where the owner did not process KYC verification.
  INPUT_NAME_MISMATCHED = 'INPUT_NAME_MISMATCHED', //The name of the addressee included in the request message is different from the name of the actual owner.
  SANCTION_LIST = 'SANCTION_LIST', //Virtual asset addresses or owners are subject to the sanction of the beneficiary VASP.
  LACK_OF_INFORMATION = 'LACK_OF_INFORMATION', //This is a case where there is no the information necessary to make an asset transfer decision.
  UNKNOWN = 'UNKNOWN', //This refers to other reasons.
}

export type VirtualAssetAddressSearchResponse = {
  result: VirtualAssetAddressSearchResult;
  reasonType?: VirtualAssetAddressSearchInvalidReasonType;
  reasonMsg?: string;
  beneficiaryVaspEntityId: string;
};

export type AssetTransferAuthorizationBody = {
  transferId: string;
  currency: string;
  amount: string;
  historicalCost?: string;
  tradePrice: string;
  tradeCurrency: string;
  isExceedingThreshold: string;
  originatingVASP?: any;
  payload: {
    ivms101: {
      Beneficiary: {
        beneficiaryPersons: any[];
        accountNumber: string[];
      };
    };
  };
};

export enum AssetTransferAuthorizationResult {
  verified = 'verified',
  denied = 'denied',
}
export enum AssetTransferAuthorizationReasonType {
  NOT_FOUND_ADDRESS = 'NOT_FOUND_ADDRESS', //This is a case where a virtual asset address cannot be found.
  NOT_SUPPORTED_SYMBOL = 'NOT_SUPPORTED_SYMBOL', //This is a currency symbol which cannot be traded.
  NOT_KYC_USER = 'NOT_KYC_USER', //This is a case where the owner did not process KYC verification.
  INPUT_NAME_MISMATCHED = 'INPUT_NAME_MISMATCHED', //The name of the addressee included in the request message is different from the name of the actual owner.
  SANCTION_LIST = 'SANCTION_LIST', //Virtual asset addresses or owners are subject to the sanction of the beneficiary VASP.
  LACK_OF_INFORMATION = 'LACK_OF_INFORMATION', //This is a case where there is no the information necessary to make an asset transfer decision.
  UNKNOWN = 'UNKNOWN', //This refers to other reasons.
}

export type AssetTransferAuthorizationResponse = {
  result: AssetTransferAuthorizationResult;
  reasonType?: AssetTransferAuthorizationReasonType;
  reasonMsg?: string;
  transferId: string;
  payload: {
    ivms101: {
      Originator: {
        originatorPersons: any[];
        accountNumber: string[];
        customerIdentification: string;
      };
      Beneficiary: {
        beneficiaryPersons: any[];
        accountNumber: string[];
        customerIdentification: string;
      };
      OriginatingVASP: OriginatingVASP;
      BeneficiaryVASP: BeneficiaryVASP;
    };
  };
};

type OriginatingVASP = {
  originatingVASP: VASP;
};
type BeneficiaryVASP = {
  beneficiaryVASP: VASP;
};

type VASP = {
  legalPerson: {
    name: {
      nameIdentifier: NameIdentifier[];
    };
    geographicAddress: GeographicAddress[];
    nationalIdentification: {
      nationalIdentifier: string;
      nationalIdentifierType: string;
      registrationAuthority: string;
    };
    countryOfRegistration: string;
  };
};

type NameIdentifier = {
  legalPersonName: string;
  legalPersonNameIdentifierType: string;
};

type GeographicAddress = {
  addressType: string;
  townName: string;
  addressLine: string[];
  country: string;
};

export type ReportTransferResultBody = {
  transferId: string;
  txid: string;
  vout?: string;
};

export type TransactionStatusSearchBody = {
  transferId: string;
};

export type FinishTransferBody = {
  transferId: string;
  status: string; // Is this an ENUM?
  reasonType: string; //Is this an ENUM?
};
