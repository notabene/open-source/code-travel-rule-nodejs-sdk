import { describe, beforeAll, it, expect } from 'vitest';
import CodeVaspAPI from '../code-vasp-api';
import { CodeTravelRuleConfig } from '../../code-travel-rule';
describe('code-vasp-api', () => {
  let sut: CodeVaspAPI;

  beforeAll(() => {
    const config: CodeTravelRuleConfig = {
      allianceName: 'notabene',
      vaspSecretSeed: 'Txk6TGhO00LLvzzFeRhb4Mbi5bUjwSRQo0jIs4wPVcc=',
    };
    sut = new CodeVaspAPI(config);
  });
  it('should checkSignature()', async () => {
    /*

POST /nb-test-a/v1/beneficiary/VerifyAddress HTTP/1.1
Host: ajunge-dev-nb-code.ngrok.io
User-Agent: okhttp/4.10.0
Content-Length: 199
Accept-Encoding: gzip
Content-Type: application/json
X-Code-Req-Datetime: 2023-07-19T16:41:02.198Z
X-Code-Req-Nonce: 3532811721
X-Code-Req-Pubkey: LOpv3Vd7PKLrlDmk/MFi6mc2rPWhi3G0H3D74dayxSo=
X-Code-Req-Signature: 4f/J/iGqIqkkFQj3DYYdwNe0+yGeqTR4paxP/EFR4tU9qnZhNu0K64HW2u/JSK4M4X75s2Awv3bEJtKkEO2xCw==
X-Forwarded-For: 3.35.100.55
X-Forwarded-Proto: https
X-Request-Origin: notabene:nb-test-a

{"currency":"ETH","payload":"3hMWVEbd9c2vOhbYiogNxExx/ebHbZ0zolJTriJ61YR9qajsILmq+78BJOwBOD/nNpJk01Y54kOtaDcC1SUc7dWpelilLCINzK6qqtQdk8XpIIkn+bKYfPeUfvbxeRsFBxelRb1X4BrI3OTu6HhOQj+6X+DdZS8x0sLwPL4o"} 

*/
    const headers = {
      'X-Code-Req-Datetime': '2023-07-19T16:41:02.198Z',
      'X-Code-Req-Nonce': '3532811721',
      'X-Code-Req-Pubkey': 'LOpv3Vd7PKLrlDmk/MFi6mc2rPWhi3G0H3D74dayxSo=',
      'X-Code-Req-Signature':
        '4f/J/iGqIqkkFQj3DYYdwNe0+yGeqTR4paxP/EFR4tU9qnZhNu0K64HW2u/JSK4M4X75s2Awv3bEJtKkEO2xCw==',
      'X-Request-Origin': 'notabene:nb-test-a',
    };
    const body =
      '{"currency":"ETH","payload":"3hMWVEbd9c2vOhbYiogNxExx/ebHbZ0zolJTriJ61YR9qajsILmq+78BJOwBOD/nNpJk01Y54kOtaDcC1SUc7dWpelilLCINzK6qqtQdk8XpIIkn+bKYfPeUfvbxeRsFBxelRb1X4BrI3OTu6HhOQj+6X+DdZS8x0sLwPL4o"}';

    expect(sut.checkSignature(headers, body)).toBeTruthy();
  });

  it('should virtualAssetAddressSearch_decrypt()', async () => {
    const headers = {
      'X-Code-Req-Datetime': '2023-07-19T16:41:02.198Z',
      'X-Code-Req-Nonce': '3532811721',
      'X-Code-Req-Pubkey': 'LOpv3Vd7PKLrlDmk/MFi6mc2rPWhi3G0H3D74dayxSo=',
      'X-Code-Req-Signature':
        '4f/J/iGqIqkkFQj3DYYdwNe0+yGeqTR4paxP/EFR4tU9qnZhNu0K64HW2u/JSK4M4X75s2Awv3bEJtKkEO2xCw==',
      'X-Request-Origin': 'notabene:nb-test-a',
    };
    const body = JSON.parse(
      '{"currency":"ETH","payload":"3hMWVEbd9c2vOhbYiogNxExx/ebHbZ0zolJTriJ61YR9qajsILmq+78BJOwBOD/nNpJk01Y54kOtaDcC1SUc7dWpelilLCINzK6qqtQdk8XpIIkn+bKYfPeUfvbxeRsFBxelRb1X4BrI3OTu6HhOQj+6X+DdZS8x0sLwPL4o"}'
    );
    const decryptedBody = sut.virtualAssetAddressSearch_decrypt(headers, body);
    expect(decryptedBody).toEqual({
      currency: 'ETH',
      payload: {
        ivms101: {
          Beneficiary: {
            beneficiaryPersons: [],
            accountNumber: ['fake-address'],
          },
        },
      },
    });
  });
});
