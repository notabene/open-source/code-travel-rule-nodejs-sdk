import {
  VirtualAssetAddressSearchInvalidReasonType,
  VirtualAssetAddressSearchResult,
} from '../resources/code-api';
import { describe, beforeAll, it, expect } from 'vitest';
import { CodeTravelRule } from '..';

describe('CodeTravelRule', () => {
  let code: CodeTravelRule;

  beforeAll(() => {
    code = new CodeTravelRule({
      baseURL: 'https://localhost:8443/v1/nota-code', //'https://trapi-dev.codevasp.com',
      skipSSLcheck: true,
      allianceName: 'notabene',
      vaspEntityId: 'nb-test-a',
      vaspSecretSeed: 'Txk6TGhO00LLvzzFeRhb4Mbi5bUjwSRQo0jIs4wPVcc=',
    });
  });

  it.skip('should get the list of VASPs', async () => {
    const resp = await code.api.vaspListSearch();
    //console.log(resp);
    expect(resp.vasps).to.be.a('array');
    if (resp.vasps.length > 0) {
      const vasp = resp.vasps[0];
      expect(vasp.allianceName).to.be.a('string');
      expect(vasp.countryOfRegistration).to.be.a('string');
      expect(vasp.health).to.be.a('string');
      expect(vasp.pubkeys[0].expiresAt).to.be.a('string');
      expect(vasp.pubkeys[0].pubkey).to.be.a('string');
      expect(vasp.vaspEntityId).to.be.a('string');
      expect(vasp.vaspName).to.be.a('string');
    }
  });

  it.skip('should get public keys of a VASP', async () => {
    const resp = await code.api.publicKeyListSearch('aqx');
    console.log(resp);
    expect(resp.pubkeys).to.be.a('array');
    if (resp.pubkeys.length > 0) {
      expect(resp.pubkeys[0].expiresAt).to.be.a('string');
      expect(resp.pubkeys[0].pubkey).to.be.a('string');
    }
  });

  it.skip('should confirm an address on a VASP', async () => {
    const body = {
      currency: 'BTC',
      payload: {
        ivms101: {
          Beneficiary: {
            beneficiaryPersons: [],
            accountNumber: ['2NG9eDbW15EYVFv9BYdPSttdeLmH84c4kBx'],
          },
        },
      },
    };
    const resp = await code.api.virtualAssetAddressSearch('aqx', body);
    expect(resp.beneficiaryVaspEntityId).to.equal('aqx');
    expect(resp.result).to.equal(VirtualAssetAddressSearchResult.invalid);
    expect(resp.reasonType).to.equal(
      VirtualAssetAddressSearchInvalidReasonType.NOT_FOUND_ADDRESS
    );
  });

  it.skip('should request authorization to transfer virtual assets', async () => {
    const body = {
      transferId: '1234567890',
      currency: 'BTC',
      amount: '1.1',
      tradePrice: '26789.01',
      tradeCurrency: 'USD',
      isExceedingThreshold: 'false',
      payload: {
        ivms101: {
          Beneficiary: {
            beneficiaryPersons: [],
            accountNumber: ['2NG9eDbW15EYVFv9BYdPSttdeLmH84c4kBx'],
          },
        },
      },
    };
    const resp = await code.api.assetTransferAuthorization('aqx', body);
    expect(resp).to.equal({});
  });

  it.skip('should send a transfer result report', async () => {
    const body = {
      transferId: '1234567890',
      txid: '1095b65198584f31a577f1196f1a274d601418c17909eecf39201af8320d224e',
    };
    const resp = await code.api.reportTransferResult('aqx', body);
    expect(resp).to.equal({});
  });

  it.skip('should query a transfer status', async () => {
    const body = {
      transferId: '1234567890',
    };
    const resp = await code.api.transactionStatusSearch('aqx', body);
    expect(resp).to.equal({});
  });

  it.skip('should finish a transfer', async () => {
    const body = {
      transferId: '1234567890',
      status: 'canceled',
      reasonType: 'SANCTION_LIST',
    };
    const resp = await code.api.transactionStatusSearch('aqx', body);
    expect(resp).to.equal({});
  });
});
