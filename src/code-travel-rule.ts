import { AxiosInstance } from 'axios';
import { createClient } from './utils/create-client';
import CodeAPI from './resources/code-api';
import CodeVaspAPI from './resources/code-vasp-api';

export type CodeTravelRuleConfig = {
  allianceName: string;
  vaspSecretSeed: string;
  vaspEntityId?: string;

  baseURL?: string;

  userAgentHeader?: string;
  skipSSLcheck?: boolean;
};

const defaults: Partial<CodeTravelRuleConfig> = {
  baseURL: 'https://trapi.codevasp.com',
  userAgentHeader: 'notabene/code-travel-rule',
  skipSSLcheck: false,
};

export class CodeTravelRule {
  public http: AxiosInstance;
  public api: CodeAPI;
  public vaspApi: CodeVaspAPI;

  constructor(config: CodeTravelRuleConfig) {
    const configWithDefaults = { ...defaults, ...config };
    this.http = createClient(configWithDefaults);

    this.api = new CodeAPI(this.http, config);

    this.vaspApi = new CodeVaspAPI(config);
  }
}
