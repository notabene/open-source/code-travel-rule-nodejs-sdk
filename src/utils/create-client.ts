import axios, { InternalAxiosRequestConfig } from 'axios';
import https from 'https';
import { CodeTravelRuleConfig } from '../code-travel-rule';
import freezeSys from './freeze-object';
import { getKeysFromSeed, getVerifyKey, sign } from './code-crypto';

const codeHeadersInterceptor = (
  req: InternalAxiosRequestConfig<any>,
  config: CodeTravelRuleConfig
) => {
  // https://codevasp.gitbook.io/code-api-doc-en/api-reference/header-parameter

  //Get public and private key from sender
  const vaspKey = getKeysFromSeed(config.vaspSecretSeed);

  //X-Code-Req-Nonce must be 0 <= X-Code-Req-Nonce <= 4294967295.
  const nonce = Math.floor(Math.random() * 4294967295);
  const datetime = new Date().toISOString();

  const datetimeBuffer = Buffer.from(datetime, 'utf8');
  const nonceBuffer = new Uint8Array(new Uint32Array([nonce]).buffer).reverse();

  let toSignArray;
  if (!req.data) {
    toSignArray = Buffer.concat([datetimeBuffer, nonceBuffer]);
  } else {
    const bodyBuffer = Buffer.from(req.data, 'utf8');
    toSignArray = Buffer.concat([datetimeBuffer, bodyBuffer, nonceBuffer]);
  }

  //SignEdDSA(X-Code-Req-Datetime, URL path, body, X-Code-Req-Nonce) with secKey
  const signature = sign(toSignArray, vaspKey.secretKey);

  const pubKey = getVerifyKey(vaspKey.publicKey);

  req.headers = {
    ...req.headers,
    'X-Code-Req-Nonce': nonce,
    'X-Code-Req-PubKey': pubKey,
    'X-Code-Req-Signature': Buffer.from(signature).toString('base64'),
    'X-Code-Req-Datetime': datetime,
    'X-Request-Origin': config.allianceName + ':' + config.vaspEntityId,
  } as any;

  return req;
};

const errorInterceptor = (error: any) => {
  return Promise.reject({
    req: {
      url: '' + error?.response?.config?.baseURL + error?.response?.config?.url,
      method: error?.request?.method,
    },
    status: error?.response?.status,
    statusText: error?.response?.statusText,
    err: JSON.stringify(
      error?.response?.data?.err || error?.response?.data || error
    ),
  });
};

axios.interceptors.request.use(
  (res) => res,
  (error) => errorInterceptor(error)
);
axios.interceptors.response.use(
  (res) => res,
  (error) => errorInterceptor(error)
);

export function createClient(config: CodeTravelRuleConfig) {
  const client = axios.create({
    baseURL: config.baseURL,
    headers: {
      'Content-Type': 'application/json',
      'X-Code-SDK': 'notabene/code-travel-rule',
    },
    httpsAgent: new https.Agent({
      rejectUnauthorized: !config.skipSSLcheck,
    }),
  });

  client.interceptors.request.use(
    (res) => res,
    (error) => errorInterceptor(error)
  );
  client.interceptors.response.use(
    (res) => res,
    (error) => errorInterceptor(error)
  );

  client.interceptors.response.use((response) => {
    return freezeSys(response.data);
  });

  client.interceptors.request.use((req) => {
    return codeHeadersInterceptor(req, config);
  });
  return client;
}
