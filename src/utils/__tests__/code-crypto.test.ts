import { describe, beforeAll, it, expect } from 'vitest';
import {
  genKeyPair,
  getKeysFromSeed,
  sign,
  getVerifyKey,
  verify,
  getPublicKey,
  encrypt,
  decrypt,
  convertPublicKey,
  convertSecretKey,
} from '../code-crypto';

describe('code-crypto', () => {
  let testKeyPair;
  const vaspSecretSeed = 'Txk6TGhO00LLvzzFeRhb4Mbi5bUjwSRQo0jIs4wPVcc=';

  beforeAll(() => {
    testKeyPair = genKeyPair();
  });

  it('should gen key pair', async () => {
    expect(testKeyPair.pubKey).toBeDefined();
    expect(testKeyPair.secKey).toBeDefined();
  });

  it('should sign and verify', async () => {
    //X-Code-Req-Nonce must be 0 <= X-Code-Req-Nonce <= 4294967295.
    const nonce = Math.floor(Math.random() * 4294967295);
    const datetime = new Date().toISOString();

    const datetimeBuffer = Buffer.from(datetime, 'utf8');
    const nonceBuffer = new Uint8Array(
      new Uint32Array([nonce]).buffer
    ).reverse();

    const toSignArray = Buffer.concat([datetimeBuffer, nonceBuffer]);

    //SignEdDSA(X-Code-Req-Datetime, URL path, body, X-Code-Req-Nonce) with secKey
    const signature = sign(
      toSignArray,
      Buffer.from(testKeyPair.secKey, 'base64')
    );

    const verifyKey = getVerifyKey(Buffer.from(testKeyPair.pubKey, 'base64'));

    const pubKey = getPublicKey(verifyKey);
    //Verify
    expect(verify(toSignArray, signature, pubKey)).toBeTruthy();
  });

  it('should sign and verify (hard coded)', async () => {
    const nonce = 3532811721;
    const datetime = '2023-07-19T16:41:02.198Z';
    const bodyData =
      '{"currency":"ETH","payload":"3hMWVEbd9c2vOhbYiogNxExx/ebHbZ0zolJTriJ61YR9qajsILmq+78BJOwBOD/nNpJk01Y54kOtaDcC1SUc7dWpelilLCINzK6qqtQdk8XpIIkn+bKYfPeUfvbxeRsFBxelRb1X4BrI3OTu6HhOQj+6X+DdZS8x0sLwPL4o"}';
    const datetimeBuffer = Buffer.from(datetime, 'utf8');
    const nonceBuffer = new Uint8Array(
      new Uint32Array([nonce]).buffer
    ).reverse();

    const bodyBuffer = Buffer.from(bodyData, 'utf8');
    const toSignArray = Buffer.concat([
      datetimeBuffer,
      bodyBuffer,
      nonceBuffer,
    ]);
    const vaspKey = getKeysFromSeed(vaspSecretSeed);

    const signature = sign(toSignArray, vaspKey.secretKey);

    const xCodeReqSignature =
      '4f/J/iGqIqkkFQj3DYYdwNe0+yGeqTR4paxP/EFR4tU9qnZhNu0K64HW2u/JSK4M4X75s2Awv3bEJtKkEO2xCw==';

    expect(Buffer.from(signature).toString('base64')).toEqual(
      xCodeReqSignature
    );
    const xCodeReqPubkey = 'LOpv3Vd7PKLrlDmk/MFi6mc2rPWhi3G0H3D74dayxSo=';

    const verifyKey = getVerifyKey(Buffer.from(xCodeReqPubkey, 'base64'));

    const pubKey = getPublicKey(verifyKey);
    //Verify
    expect(
      verify(toSignArray, Buffer.from(xCodeReqSignature, 'base64'), pubKey)
    ).toBeTruthy();
  });

  it('should encrypt and decrypt', async () => {
    const testPayload = { test_field: 'test_value' };

    //Convert signing keys to encryption keys
    console.log(testKeyPair.pubKey);
    const theirDHPublicKey = convertPublicKey(testKeyPair.pubKey);
    const myDHSecretKey = convertSecretKey(testKeyPair.secKey);

    const payloadStr = JSON.stringify(testPayload);

    const encryptedPayload = encrypt(
      payloadStr,
      theirDHPublicKey as unknown as Uint8Array,
      myDHSecretKey
    );

    const encryptedPayloadStr =
      Buffer.from(encryptedPayload).toString('base64');

    //Decrypt payload
    const decryptedPayload = decrypt(
      encryptedPayloadStr,
      theirDHPublicKey as unknown as Uint8Array,
      myDHSecretKey
    );

    expect(JSON.parse(decryptedPayload)).toEqual(testPayload);
  });
});
