import nacl from 'tweetnacl';
import naclUtil from 'tweetnacl-util';
import ed2curve from 'ed2curve';

export const genKeyPair = () => {
  const keyPair = nacl.sign.keyPair();
  return {
    pubKey: Buffer.from(keyPair.publicKey).toString('base64'),
    secKey: Buffer.from(keyPair.secretKey).toString('base64'),
  };
};

export const getKeysFromSeedStr = (vaspSecretSeed: string) => {
  const keyPair = nacl.sign.keyPair.fromSeed(
    Buffer.from(vaspSecretSeed, 'base64')
  );

  return {
    pubKey: Buffer.from(keyPair.publicKey).toString('base64'),
    secKey: Buffer.from(keyPair.secretKey).toString('base64'),
  };
};

export const getKeysFromSeed = (vaspSecretSeed: string) => {
  const seed = naclUtil.decodeBase64(vaspSecretSeed);
  return nacl.sign.keyPair.fromSeed(seed);
};

export const convertPublicKey = (peerPublicKey: string) => {
  const peerVerifyKey = naclUtil.decodeBase64(peerPublicKey);
  return ed2curve.convertPublicKey(peerVerifyKey);
};
export const convertSecretKey = (secretKey: Uint8Array) => {
  return ed2curve.convertSecretKey(secretKey);
};

export const getVerifyKey = (publicKey: Uint8Array) => {
  return naclUtil.encodeBase64(publicKey);
};
export const getPublicKey = (verifyKey: string) => {
  return naclUtil.decodeBase64(verifyKey);
};

export const sign = (data: Uint8Array, secretKey: Uint8Array) => {
  return nacl.sign.detached(data, secretKey);
};

export const verify = (
  data: Uint8Array,
  signature: Uint8Array,
  publicKey: Uint8Array
) => {
  return nacl.sign.detached.verify(data, signature, publicKey);
};

export const encrypt = (
  data: string,
  peerPublicKey: Uint8Array,
  secretKey: Uint8Array
) => {
  const nonce = nacl.randomBytes(nacl.box.nonceLength);
  const messageUint8 = naclUtil.decodeUTF8(data);
  const encryptedResult = nacl.box(
    messageUint8,
    nonce,
    peerPublicKey,
    secretKey
  );

  // actual data to send (prepend nonce)
  const encryptedWithNonce = Buffer.concat([nonce, encryptedResult]);
  return encryptedWithNonce;
};

export const decrypt = (
  data: string,
  peerPublicKey: Uint8Array,
  privateKey: Uint8Array
) => {
  const encryptedData = Buffer.from(data, 'base64');
  const nonce = encryptedData.slice(0, nacl.box.nonceLength);
  const rawEncrypted = encryptedData.slice(nacl.box.nonceLength);

  // Decryption
  const decrypted = nacl.box.open(
    rawEncrypted,
    nonce,
    peerPublicKey,
    privateKey
  );
  if (decrypted === null) {
    throw new Error('unable to decrypt');
  }
  return naclUtil.encodeUTF8(decrypted);
};
