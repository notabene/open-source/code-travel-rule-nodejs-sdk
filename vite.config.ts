/// <reference types="vitest" />
import { defineConfig } from 'vite';

export default defineConfig({
  test: {
    globals: true,
    environment: 'node',
    exclude: [
      '.trunk',
      '.husky',
      '.parcel-cache',
      '.vscode',
      '.yarn',
      'dist',
      'node_modules',
      '.git',
    ],
    coverage: {
      provider: 'c8',
      reporter:
        process.env.ENVIRONMENT !== 'ci'
          ? ['text', 'text-summary']
          : ['text', 'text-summary', 'cobertura'],
      exclude: [
        '.docker/*',
        'migrations/*',
        'setupTests.ts',
        '**/.git/**',
        '.trunk',
        'node_modules',
        'dist',
        'src/entities/*',
        'src/utils/datadog.ts',
        'src/utils/logger.ts',
        'src/api/errors.ts',
        '.yarn',
        'src/**/*.test.ts',
        'config/*',
      ],
    },
  },
});
