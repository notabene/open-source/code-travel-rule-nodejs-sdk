<div align="center">

<img src="https://www.codevasp.com/images/logo-code.png" height=50>

<br>

# CODE Travel Rule NodeJS SDK

[![pipeline status](https://gitlab.com/notabene/open-source/code-travel-rule-nodejs-sdk/badges/master/pipeline.svg)](https://gitlab.com/notabene/open-source/code-travel-rule-nodejs-sdk/-/commits/master)
[![Latest Release](https://gitlab.com/notabene/open-source/code-travel-rule-nodejs-sdk/-/badges/release.svg)](https://gitlab.com/notabene/open-source/code-travel-rule-nodejs-sdk/-/releases)

NodeJS compatible library to connect to CODE (codevasp.com) Travel Rule platform

[Getting started](#getting-started) •
[Installation](#installation) •
[Configuration](#configuration)

</div>

## Getting Started

### Step 1: Install the library

```
npm install @notabene/code-travel-rule
```

### Step 2: Initialize the client

```javascript
const { CodeTravelRule } = require('@notabene/code-travel-rule');

const code = new CodeTravelRule({
  allianceName: '{ALLIANCE_NAME}', //Add your own alliance name
  vaspEntityId: '{VASP_ENTITY_ID}', // Add your own vasp entity ID
  vaspSecretSeed: '{VASP_SECRET_SEED}', // Add your own secret seet
});
```

## Configuration

### Alliance Name, VASP Entity ID and VASP Secret Seed

- Alliance Name is the name of the alliance this VASP belongs. Example: `notabene`.
- The VASP Entity ID is the id provided by CODE when the VASP is onboarded as a member.
- The VASP secret seed is the Base64 encoded seed used to generate the private and public keys.

### Test environment

For using the test environment of CODE please set the `baseURL` to the corresponding endpoint

```javascript
onst { CodeTravelRule } = require('@notabene/code-travel-rule');

const code = new CodeTravelRule({
  allianceName: '{ALLIANCE_NAME}', //Add your own alliance name
  baseURL: 'trapi-dev.codevasp.com',
  vaspEntityId: '{VASP_ENTITY_ID}', // Add your own vasp entity ID
  vaspSecretSeed: '{VASP_SECRET_SEED}', // Add your own secret seet
});
```

## API methods

https://codevasp.gitbook.io/code-api-doc-en/code-api/

- `code.api.vaspListSearch()` returns a list of all VASPs in CODE network
- `code.api.publicKeyListSearch(vaspEntityId)` returns a list of all public keys for a VASP
- `code.api.virtualAssetAddressSearch(vaspEntityId,body)` Request confirmation of address ownership
- `code.api.assetTransferAuthorization(vaspEntityId,body)` Request an authorization for the transfer of virtual assets
- `code.api.reportTransferResult(vaspEntityId,body)` Report result of the blockchain transaction
- `code.api.transactionStatusSearch(originatingVaspEntityId,body)` Request the blockchain transaction status
- `code.api.finishTransfer(originatingVaspEntityId,body)` Request the counterpart VASP to change the status

## Appendix

### Example 1

## [License](LICENSE.md)

BSD 3-Clause © Notabene Inc.
