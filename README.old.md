# Library

## Description

This repository is a template for a Notabene TypeScript library. It includes:

- TypeScript setup
- Yarn 3
- Linting (ESLint & Prettier)
- Testing (Jest)
- Publishing to NPM (Semantic-Release)
- GitLab pipelines

## GitLab Project Setup

1. Start a new project in our [GitLab libraries group](https://gitlab.com/notabene/libraries) and select "Create from template"
2. Select the "Group" tab
3. Next to "Library" click "Use template"
4. Select a project name and click "Create project"
5. Setup a new project access token by going to `Settings` > `Access Tokens` and generating a new one with just the `api` permission.
6. Create a new CI/CD variable by going to `Settings` > `CI/CD` > `Variables` and adding a new one called `GITLAB_TOKEN` with the

## Library Setup

1. Setup `package.json` by updating the `name`, `description`, `homepage` and `repository.url`.
2. Update this `README.md` to document your new library

## Editor Setup

If using VSCode make sure to set the TypeScript version to the workspace version. Bring up the command pallete and search for "Select TypeScript Version...", then select "Use Workspace Version".

Then run the following to setup VSCode for TS with linting support: `yarn dlx @yarnpkg/sdks vscode`

Finally, install the `ZipFS` extension which allows VSCode to inspect and modify Yarn dependencies that are stored in `.zip` format.

## Usage

- `yarn` - To install dependencies
- `yarn watch` - Hot reloading for development
- `yarn build` - Compile the library to the `dist/` folder
- `yarn lint` - Lints the library using ESLint/Prettier
- `yarn test` - Run Jest to test the library
- `yarn release` - Release the project using semantic-release (only run in CI)

## Dependencies

This template publishes to the GitLab package repository (instead of NPM). To install packages from there add the following to an `.npmrc` file:

```
//gitlab.com/api/v4/packages/npm/:_authToken=${GITLAB_TOKEN}
@notabene:registry=https://gitlab.com/api/v4/packages/npm/
```

OR if you're using a project with Yarn v2 or v3 then add the following to your `.yarnrc.yml` file:

```
yarnPath: .yarn/releases/yarn-3.2.1.cjs
npmScopes:
  notabene:
    npmRegistryServer: "https://gitlab.com/api/v4/packages/npm/"
    npmAuthToken: ${GITLAB_TOKEN}
```

Make sure you have `GITLAB_TOKEN` setup locally in your development environment.

First generate a new personal access token by going to [Access Tokens](https://gitlab.com/-/profile/personal_access_tokens) in GitLab and generating a new token with at least the `api` permission.

Then export the token in your environment:

```
export GITLAB_TOKEN=glpat-qY....
```

Then run `yarn add @notabene/{PACKAGE_NAME}`
